package edu.mat.alg.datastructure.binarysearchtree;

import java.util.List;

import ec.util.MersenneTwisterFast;
import edu.mat.alg.util.Util;

/**
 * Binary search tree data structure implementation.
 * */
public class BinarySearchTree {

	/**
	 * Root node of this tree.
	 * */
	private Node root;

	/**
	 * @return the root
	 */
	public final Node getRoot() {
		return root;
	}

	/**
	 * Build the binary search tree from a set of int numbers.
	 * 
	 * @param numbers list of numbers
	 * */
	public final void buildTree(final List<Integer> numbers) {
		MersenneTwisterFast random = Util.getInstance().getRandomInstance();

		// create root
		int index = random.nextInt(numbers.size());
		int value = numbers.get(index);
		System.out.println("Inserting: " + value);
		root = new Node(value);
		numbers.remove(index);

		// insert each number into tree
		while (numbers.size() > 0) {
			index = random.nextInt(numbers.size());
			value = numbers.get(index);
			System.out.println("Inserting: " + value);
			insertNode(value, root);
			numbers.remove(index);
		}
	}

	/**
	 * Recursive method to search a value in tree structure.
	 * This method could be called with root node as initial
	 * node to be searched.
	 * 
	 * @param node current node to be searched
	 * @param value the value to be searched
	 * @return the {@link Node} in question
	 * */
	private Node searchNode(final Node node, final int value) {
		Node result = null;

		if (value == node.getValue()) {
			result = node;
		} else if (value <= node.getValue()) {
			result = searchNode(node.getLeftNode(), value);
		} else if (value > node.getValue()) {
			result = searchNode(node.getRightNode(), value);
		}

		return result;
	}

	/**
	 * Get the left most node which also holds the smallest
	 * value in the tree.
	 * 
	 * @param node the initial node to start search from,
	 *        it is usually the root of the tree
	 * @return the left most node
	 * */
	public final Node getMostLeftNode(final Node node) {
		Node result = node;

		while (result.getLeftNode() != null) {
			result = result.getLeftNode();
		}

		return result;
	}

	/**
	 * Get the right most node which also holds the largest
	 * value in the tree.
	 * 
	 * @param node the initial node to start search from,
	 *        it is usually the root of the tree
	 * @return the right most node
	 * */
	public final Node getMostRightNode(final Node node) {
		Node result = node;

		while (result.getRightNode() != null) {
			result = result.getRightNode();
		}

		return result;
	}

	/**
	 * Successor is the smallest node that is greater than
	 * the node in question.
	 * 
	 * We can get the successor by getting the right node of
	 * current node and then iterating through left most node
	 * of the current node.
	 * 
	 * A successor can have one child maximum, as right node.
	 * 
	 * @param node the node which the successor is tobe searched
	 * @return the successor node
	 * */
	private Node getSuccessorNode(final Node node) {
		Node result = node;

		if (result != null) {
			// get right node of current node
			result = result.getRightNode();
			if (result != null) {
				// get the left most node of current node
				result = getMostLeftNode(result);
			}
		}

		return result;
	}

	/**
	 * Delete a node in the tree based on the value in the node.
	 * If there are more than one nodes with same value, this method
	 * only deletes one node.
	 * <br><br>
	 * Deleting a node depends on how many children it has.<br>
	 * 1. If the node has no child, we just remove it<br>
	 * 2. If the node has one child, we splice it out<br>
	 * 3. If the node has two children, we splice its successor out
	 * 
	 * @param value the value to be deleted
	 * @return boolean value whether deletion is success/fail
	 * */
	public final boolean deleteNode(final int value) {
		boolean result = false;
		Node node = searchNode(root, value);
		int totalChildren = 0;

		if (node.getLeftNode() != null) {
			totalChildren++;
		}
		if (node.getRightNode() != null) {
			totalChildren++;
		}

		if (node != null) {
			Node parent = node.getParentNode(); // get parent
			if (totalChildren == 0) {
				// if the node has no child, just remove it
				if (parent.getLeftNode() == node) {
					parent.setLeftNode(null);
				} else if (parent.getRightNode() == node) {
					parent.setRightNode(null);
				}
				node.setParentNode(null);
				node = null;
			} else if (totalChildren == 1) {
				// if the node has one child, connect its parent to its child,
				// then remove the node
				Node child = node.getLeftNode();    // get child
				if (child == null) {
					child = node.getRightNode();
				}

				child.setParentNode(parent);        // connect parent to child
				if (parent.getLeftNode() == node) {
					parent.setLeftNode(child);
				} else if (parent.getRightNode() == node) {
					parent.setRightNode(child);
				}

				node.setParentNode(null);           // remove the node
				node.setLeftNode(null);
				node.setRightNode(null);
				node = null;
			} else if (totalChildren == 2) {
				// if the node has two children, get its successor,
				// replace the node with the successor

				// get successor, its parent, & its only one child node
				Node successor = getSuccessorNode(node);
				Node successorParent = successor.getParentNode();
				Node successorChild = successor.getRightNode();

				// replace successor's position with its child,
				// connect successor's child to successor's parent
				if (successorParent.getLeftNode() == successor) {
					successorParent.setLeftNode(successorChild);
				} else if (successorParent.getRightNode() == successor) {
					successorParent.setRightNode(successorChild);
				}
				if (successorChild != null) {
					successorChild.setParentNode(successorParent);
				}

				// replace node's value with its successor
				node.setValue(successor.getValue());

				// remove the successor
				successor.setParentNode(null);
				successor.setLeftNode(null);
				successor.setRightNode(null);
				successor = null;
			}
		}

		return result;
	}

	/**
	 * Insert an int value into tree started from specified node.
	 * In binary search tree, new node is always inserted at the bottom.
	 * 
	 * @param value the int value to be inserted
	 * @param node the started node, usually started from root node
	 * @return the inserted new node
	 * */
	private Node insertNode(final int value, final Node node) {
		Node result = null;
		boolean isRightPosition = false;

		// check whether the new node should be put in left/right side
		if (value <= node.getValue()) {
			result = node.getLeftNode();
		} else {
			result = node.getRightNode();
			isRightPosition = true;
		}

		// if left/right node is null (bottom most node), then put the new node
		// there, otherwise do recursive until the correct position is found
		if (result == null) {
			result = new Node(value);
			result.setParentNode(node);
			result.setLeftNode(null);
			result.setRightNode(null);

			if (isRightPosition) {
				node.setRightNode(result);
			} else {
				node.setLeftNode(result);
			}
		} else {
			result = insertNode(value, result);
		}

		return result;
	}

	/**
	 * Print the tree recursively.
	 * 
	 * @param node the node to start from
	 * @param string the string to print
	 * */
	public final void printTree(
			final Node node, final StringBuffer string) {
		string.append(node.getValue());
		if (node.getLeftNode() != null) {
			StringBuffer sb = new StringBuffer(string);
			sb.append(" - (left)");
			printTree(node.getLeftNode(), sb);
		}
		if (node.getRightNode() != null) {
			StringBuffer sb = new StringBuffer(string);
			sb.append(" - (right)");
			printTree(node.getRightNode(), sb);
		}
		if (node.getLeftNode() == null && node.getRightNode() == null) {
			System.out.println(string);
		}
	}

}
