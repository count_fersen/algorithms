/**
 * Package contains binary search tree implementation.
 * */
package edu.mat.alg.datastructure.binarysearchtree;