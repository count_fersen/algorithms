package edu.mat.alg.datastructure.binarysearchtree;

/**
 * A class represents a node in binary tree data structure.
 * A node consists of an int value, left node, right node, and parent node.
 * In binary search tree, left node has always lower or equal
 * value than its parent's value, and right node has always bigger
 * value than its parent's value.
 * 
 * i.e. value = 5, left = 2, right = 7
 * */
public class Node {

	/**
	 * Actual number of this node.
	 * */
	private int value;

	/**
	 * Child node on left side. Left node has always lower
	 * or equal value than its parent's value.
	 * */
	private Node leftNode;

	/**
	 * Child node on right side. Right node has always
	 * bigger value than its parent's value.
	 * */
	private Node rightNode;

	/**
	 * Parent node of this node.
	 * */
	private Node parentNode;

	/**
	 * Public constructor.
	 * @param paramValue the value of this node
	 * */
	public Node(final int paramValue) {
		this.value = paramValue;
	}

	/**
	 * @return the leftNode
	 */
	public final Node getLeftNode() {
		return leftNode;
	}

	/**
	 * @param paramLeftNode the leftNode to set
	 */
	public final void setLeftNode(final Node paramLeftNode) {
		this.leftNode = paramLeftNode;
	}

	/**
	 * @return the rightNode
	 */
	public final Node getRightNode() {
		return rightNode;
	}

	/**
	 * @param paramRightNode the rightNode to set
	 */
	public final void setRightNode(final Node paramRightNode) {
		this.rightNode = paramRightNode;
	}

	/**
	 * @return the parentNode
	 */
	public final Node getParentNode() {
		return parentNode;
	}

	/**
	 * @param paramParentNode the parentNode to set
	 */
	public final void setParentNode(final Node paramParentNode) {
		this.parentNode = paramParentNode;
	}

	/**
	 * @return the value
	 */
	public final int getValue() {
		return value;
	}

	/**
	 * @param paramValue the value to set
	 */
	public final void setValue(final int paramValue) {
		this.value = paramValue;
	}

}
