/**
 * Package contains search algorithm implementations.
 * */
package edu.mat.alg.search.impl;