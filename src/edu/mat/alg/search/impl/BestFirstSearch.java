package edu.mat.alg.search.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import edu.mat.alg.search.BaseSearch;
import edu.mat.alg.search.data.City;
import edu.mat.alg.search.data.Node;

/**
 * Best-first search algorithm implementation.
 * */
public class BestFirstSearch extends BaseSearch {

	/**
	 * List of being observed nodes.
	 * */
	private List<Node> beingObservedNodes;

	/**
	 * Public constructor.
	 * 
	 * @param startCityParam city to start from
	 * @param finishCityParam city to get to
	 * */
	public BestFirstSearch(
			final City startCityParam, final City finishCityParam) {
		super(startCityParam, finishCityParam);
		beingObservedNodes = new ArrayList<Node>();
	}

	@Override
	public final void search() {
		putNodes(getStartCity());

		while (beingObservedNodes.size() > 0) {
			Node node = beingObservedNodes.get(0);

			System.out.println("Observing path: " + node.getCityFrom()
					.getCityName() + " & " + node.getCityTo().getCityName());

			if (compareCity(node.getCityFrom())
					|| compareCity(node.getCityTo())) {
				System.out.println("City has been found.");
				break;
			} else {
				beingObservedNodes.remove(node);
				putNodes(node.getCityFrom());
				putNodes(node.getCityTo());
			}
		}
	}

	/**
	 * Put nodes in a {@link City} to be observed.
	 * 
	 * @param city the {@link City} to be observed
	 * */
	private void putNodes(final City city) {
		List<Node> nodes = city.getNodes();
		Iterator<Node> iter = nodes.iterator();
		while (iter.hasNext()) {
			Node node = iter.next();
			if (!node.isExpanded() && !beingObservedNodes.contains(node)) {
				node.setExpanded(true);
				// order the node by their length
				Iterator<Node> costIter = beingObservedNodes.iterator();
				if (!costIter.hasNext()) {
					beingObservedNodes.add(0, node);
					continue;
				}
				while (costIter.hasNext()) {
					Node cost = costIter.next();
					if (cost.getLength() > node.getLength()) {
						beingObservedNodes.add(
								beingObservedNodes.indexOf(cost), node);
						break;
					} else if (!costIter.hasNext()) {
						beingObservedNodes.add(node);
						break;
					}
				}
			}
		}
	}

}
