package edu.mat.alg.search.impl.genetic.tsp;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ec.util.MersenneTwisterFast;
import edu.mat.alg.search.data.City;
import edu.mat.alg.search.data.Node;
import edu.mat.alg.util.Util;

/**
 * In TSP, a salesman must visit all cities in a map once.
 * So, the chromosome should be the path that the salesman
 * walks through.
 * 
 * i.e. the salesman have to visit 4 cities A B C and D. The
 * possible path would be ABCDA, AADCB, BCDDB, etc. Note that for x
 * cities, there will be (x + 1) cities in a path because the
 * salesman needs to go back to the first city he/she started.
 * */
public class Chromosome {

	/**
	 * This is the chance that a city within a chromosome will be mutated.
	 * Mutation can be achieved by randomly select another city.
	 * */
	private static final double MUTATION_RATE = 0.001;

	/**
	 * Fitness score for each chromosome. If path is correct, fitness score
	 * is equal to total length. If path is wrong, fitness score is 0.
	 * */
	private double fitnessScore;

	/**
	 * The actual chromosome string. Its length is total cities + 1.
	 * */
	private List<City> chromosome;

	/**
	 * TSP object for this chromosome.
	 * */
	private TravellingSalesmanProblem tsp;

	/**
	 * Public constructor.
	 * 
	 * @param salesmanProblem TSP object for this chromosome
	 * */
	public Chromosome(final TravellingSalesmanProblem salesmanProblem) {
		this.fitnessScore = 0;
		this.chromosome = new ArrayList<City>();
		this.tsp = salesmanProblem;

		// generate random binary string
		MersenneTwisterFast random = Util.getInstance().getRandomInstance();
		for (int i = 0; i < tsp.getLength(); i++) {
			int index = random.nextInt(tsp.getLength() - 1);
			chromosome.add(tsp.getCities()[index]);
		}

		calculateFitnessScore();
	}

	/**
	 * Check whether this chromosome is a valid TSP path. Note that
	 * a valid chromosome is also a correct answer.
	 * 
	 * @return true if the chromosome is valid
	 * */
	private boolean isValidChromosome() {
		// check if length is correct
		if (chromosome.size() != tsp.getLength()) {
			return false;
		}

		// check if first city == last city
		if (chromosome.get(0) != chromosome.get(chromosome.size() - 1)) {
			return false;
		}

		// check if a city is listed only once
		for (int i = 1; i < chromosome.size() - 1; i++) {
			City city = chromosome.get(i);
			for (int j = i + 1; j < chromosome.size(); j++) {
				if (city == chromosome.get(j)) {
					return false;
				}
			}
		}

		// iterate through all cities in chromosome
		Iterator<City> iter = chromosome.iterator();
		City startCity = iter.next();
		while (iter.hasNext()) {
			City nextCity = iter.next();

			// if there is no connection from start city, return false
			List<Node> nodes = startCity.getNodes();
			if (nodes == null || nodes.size() == 0) {
				return false;
			}

			// iterate through all nodes and check whether next city is correct
			boolean isFound = false;
			Iterator<Node> nodeIter = startCity.getNodes().iterator();
			while (nodeIter.hasNext()) {
				Node node = nodeIter.next();
				String cityFrom = node.getCityFrom().getCityName();
				String cityTo = node.getCityTo().getCityName();

				if ((cityFrom.equals(startCity.getCityName())
						&& cityTo.equals(nextCity.getCityName()))
					|| (cityFrom.equals(nextCity.getCityName())
						&& cityTo.equals(startCity.getCityName()))) {
					startCity = nextCity;
					isFound = true;
					break;
				} else {
					isFound = false;
				}
			}

			if (!isFound) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Get fitness score of the chromosome.
	 * <br>
	 * Any valid chromosome (chromosome with correct path) has
	 * fitness score = total node's length
	 * <br>
	 * Any invalid chromosome (chromosome with incorrect path) has
	 * zero fitness score.
	 * */
	public final void calculateFitnessScore() {
		fitnessScore = 0;
		if (isValidChromosome()) {
			// iterate through all cities in chromosome and sum node's length
			Iterator<City> iter = chromosome.iterator();
			City startCity = iter.next();
			while (iter.hasNext()) {
				City nextCity = iter.next();
				Iterator<Node> nodeIter = startCity.getNodes().iterator();
				while (nodeIter.hasNext()) {
					Node node = nodeIter.next();
					String cityFrom = node.getCityFrom().getCityName();
					String cityTo = node.getCityTo().getCityName();

					if ((cityFrom.equals(startCity.getCityName())
							&& cityTo.equals(nextCity.getCityName()))
						|| (cityFrom.equals(nextCity.getCityName())
							&& cityTo.equals(startCity.getCityName()))) {
						fitnessScore = fitnessScore + node.getLength();
						startCity = nextCity;
						break;
					}
				}
			}
			System.out.println("Chromosome: " + this);
		}
	}

	/**
	 * Mutate a chromosome. We mutate chromosome simply by changing a city
	 * by another random city.
	 * */
	public final void mutate() {
		MersenneTwisterFast random = Util.getInstance().getRandomInstance();
		for (int i = 0; i < chromosome.size(); i++) {
			if (random.nextDouble() <= MUTATION_RATE) {
				int index = random.nextInt(tsp.getLength() - 1);
				chromosome.set(i, tsp.getCities()[index]);
			}
		}
	}

	/**
	 * @return the chromosome
	 */
	public final List<City> getChromosome() {
		return chromosome;
	}

	/**
	 * @return the fitnessScore
	 */
	public final double getFitnessScore() {
		return fitnessScore;
	}

	@Override
	public final String toString() {
		StringBuffer sb = new StringBuffer();
		Iterator<City> iter = chromosome.iterator();
		while (iter.hasNext()) {
			sb.append('[');
			sb.append(iter.next().getCityName());
			sb.append(']');
		}
		sb.append(" Fitness score: ");
		sb.append(fitnessScore);

		return sb.toString();
	}

}
