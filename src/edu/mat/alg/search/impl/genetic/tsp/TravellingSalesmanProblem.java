package edu.mat.alg.search.impl.genetic.tsp;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ec.util.MersenneTwisterFast;
import edu.mat.alg.search.ISearch;
import edu.mat.alg.search.data.City;
import edu.mat.alg.util.Util;

/**
 * Travelling salesman problem implementation. The generated chromosome
 * is not guaranteed to be a valid path.
 * */
public class TravellingSalesmanProblem implements ISearch {

	/**
	 * Total population of the chromosomes.
	 * */
	private static final int TOTAL_POPULATION = 100;

	/**
	 * Total generation.
	 * */
	private static final int TOTAL_GENERATION = 1000;

	/**
	 * This is simply the chance that two chromosomes will swap their bits.
	 * */
	private static final double CROSS_RATE = 0.7;

	/**
	 * All cities that must be travelled by salesman.
	 * */
	private City[] cities;

	/**
	 * List of chromosomes.
	 * */
	private List<Chromosome> chromosomes;

	/**
	 * Public constructor.
	 * 
	 * @param paramCities all cities that must be travelled by salesman
	 * */
	public TravellingSalesmanProblem(final City[] paramCities) {
		this.chromosomes = new ArrayList<Chromosome>();
		this.cities = paramCities;
	}

	@Override
	public final void search() {
		// generate new population
		generatePopulation();

		List<Chromosome> newPopulation = new ArrayList<Chromosome>();
		int generation = 0;

		while (generation < TOTAL_GENERATION) {
			newPopulation.clear();
			generation++;
			for (int i = 0; i < TOTAL_POPULATION; i = i + 2) {
				// select two members
				Chromosome chromosome1 = selectChromosome();
				Chromosome chromosome2 = selectChromosome();

				// crossover bits
				crossOverChromosomes(chromosome1, chromosome2);

				// mutate chromosomes
				chromosome1.mutate();
				chromosome2.mutate();

				// recalculate fitness score
				chromosome1.calculateFitnessScore();
				chromosome2.calculateFitnessScore();

				// put new chromosomes into pool
				newPopulation.add(chromosome1);
				newPopulation.add(chromosome2);
			}
			chromosomes.addAll(newPopulation);
		}
	}

	/**
	 * Generate first population.
	 * */
	private void generatePopulation() {
		for (int i = 0; i < TOTAL_POPULATION; i++) {
			Chromosome chromosome = new Chromosome(this);
			chromosomes.add(chromosome);
		}
	}

	/**
	 * Select a chromosome from the population pool.
	 * The chance of being selected is proportional to its fitness.
	 * We use Roulette Wheel to select a chromosome.
	 * 
	 * @return the selected chromosome
	 * */
	private Chromosome selectChromosome() {
		Chromosome chromosome = null;
		double totalFitnessScore = 0.0;
		double totalSlicedScore = 0.0;
		double slice = 0.0;

		// calculate total fitness score
		Iterator<Chromosome> iterator = chromosomes.iterator();
		while (iterator.hasNext()) {
			Chromosome obj = iterator.next();
			totalFitnessScore = totalFitnessScore + obj.getFitnessScore();
		}

		// randomly put a slice
		slice = totalFitnessScore * Util.getInstance().
		getRandomInstance().nextDouble();

		// get the chromosome where slice has been put
		iterator = chromosomes.iterator();
		while (iterator.hasNext()) {
			Chromosome obj = iterator.next();
			totalSlicedScore = totalSlicedScore + obj.getFitnessScore();
			if (totalSlicedScore >= slice) {
				chromosome = obj;
				chromosomes.remove(obj);
				break;
			}
		}

		if (chromosome == null) {
			chromosome = chromosomes.remove(0);
		}

		return chromosome;
	}

	/**
	 * Crossover two chromosomes.
	 * 
	 * @param chromo1 first chromosome
	 * @param chromo2 second chromosome
	 * */
	private void crossOverChromosomes(
			final Chromosome chromo1, final Chromosome chromo2) {
		// check if we should crossover chromosomes
		MersenneTwisterFast random = Util.getInstance().getRandomInstance();
		if (random.nextDouble() > CROSS_RATE) {
			return;
		}

		// generate random position
		int position = random.nextInt(getLength());

		// swap the bits
		List<City> part1 = chromo1.getChromosome().subList(0, position);
		List<City> part2 = chromo1.getChromosome().subList(
				position, getLength());
		List<City> part3 = chromo2.getChromosome().subList(0, position);
		List<City> part4 = chromo2.getChromosome().subList(
				position, getLength());

		List<City> x = new ArrayList<City>();
		x.addAll(part1);
		x.addAll(part4);
		List<City> y = new ArrayList<City>();
		y.addAll(part3);
		y.addAll(part2);
		chromo1.getChromosome().clear();
		chromo1.getChromosome().addAll(x);
		//chromo1.getChromosome().addAll(part4);
		chromo2.getChromosome().clear();
		chromo2.getChromosome().addAll(y);
		//chromo2.getChromosome().addAll(part2);
	}

	/**
	 * Length of all cities that must be travelled by salesman.
	 * The value is total cities + 1.
	 * 
	 * @return the length
	 */
	public final int getLength() {
		return cities.length + 1;
	}

	/**
	 * @return the cities
	 */
	public final City[] getCities() {
		return cities;
	}

}
