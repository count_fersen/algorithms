/**
 * Package contains genetic search algorithm
 * implementation for travelling salesman problem (TSP).
 * */
package edu.mat.alg.search.impl.genetic.tsp;