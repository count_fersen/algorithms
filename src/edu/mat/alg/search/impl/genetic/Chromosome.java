package edu.mat.alg.search.impl.genetic;

import ec.util.MersenneTwisterFast;
import edu.mat.alg.util.Util;

/**
 * Chromosome implementation to be used in Genetic Algorithm.
 * */
public class Chromosome {

	/**
	 * Decoded characters of the chromosome.
	 * The following characters are decoded characters:
	 * 0 = 0000
	 * 1 = 0001
	 * 2 = 0010
	 * 3 = 0011
	 * 4 = 0100
	 * 5 = 0101
	 * 6 = 0110
	 * 7 = 0111
	 * 8 = 1000
	 * 9 = 1001
	 * + = 1010
	 * - = 1011
	 * * = 1100
	 * / = 1101
	 * */
	private static final String[] DECODED_CHARACTERS = new String[] {
		"0000", "0001", "0010", "0011", "0100",
		"0101", "0110", "0111", "1000", "1001",
		"1010", "1011", "1100", "1101"};

	/**
	 * Characters that make up a chromosome.
	 * */
	private static final char[] CHARACTERS = new char[] {
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '-', '*', '/'};

	/**
	 * Length of the chromosome. For example, 3-4*7 has 5 characters.
	 * */
	private static final int LENGTH = 5;

	/**
	 * This is the chance that a bit within a chromosome will be flipped
	 * (0 -> 1, 1 -> 0).
	 * */
	private static final double MUTATION_RATE = 0.001;

	/**
	 * Fitness score for each chromosome. Zero is the best fitness score.
	 * */
	private double fitnessScore;

	/**
	 * The actual chromosome string.
	 * */
	private StringBuffer chromosome;

	/**
	 * Decoded chromosome string.
	 * */
	private StringBuffer decodedChromosome;

	/**
	 * Public constructor.
	 * @param targetScore the total score desired
	 * */
	public Chromosome(final int targetScore) {
		super();
		this.fitnessScore = -1;
		this.chromosome = new StringBuffer();
		this.decodedChromosome = new StringBuffer();
		
		// generate random binary string
		MersenneTwisterFast random = Util.getInstance().getRandomInstance();
		for (int i = 0; i < LENGTH; i++) {
			int index = random.nextInt(CHARACTERS.length);
			chromosome.append(CHARACTERS[index]);
			decodedChromosome.append(DECODED_CHARACTERS[index]);
		}

		calculateFitnessScore(targetScore);
	}

	/**
	 * Get fitness score of the chromosome.
	 * <br><br>
	 * Any invalid chromosome (chromosome with wrong format or zero-devided),
	 * will have 1/targetScore fitness score.
	 * <br><br>
	 * Any valid chromosome, except the correct one, will have its own
	 * calculated fitness score.
	 * <br><br>
	 * The correct chromosome will have 0 fitness score.
	 * 
	 * @param targetScore the target score to calculate fitness score
	 * */
	public final void calculateFitnessScore(final int targetScore) {
		double total = calculateChromosomeValue();
		if (total == targetScore) { // divide by zero exception
			fitnessScore = 0;
		} else {
			fitnessScore = 1.0 / (targetScore - total);
		}
	}

	/**
	 * Get value of the chromosome. For example 3-4*7 would have value -7.
	 * Any invalid chromosome (chromosome with wrong format or zero-devided),
	 * will have 0 value.
	 * 
	 * @return value of the chromosome
	 * */
	private double calculateChromosomeValue() {
		double total = 0;

		if (isValidChromosome()) {
			for (int i = 0; i < chromosome.length(); i++) {
				double currInt = Character.getNumericValue(
						chromosome.charAt(i));
				if (i == 0) { // put first number in total
					total = currInt;
				} else if (i % 2 == 0) { // if number, operate with total
					if (chromosome.charAt(i - 1) == '+') {
						total = total + currInt;
					} else if (chromosome.charAt(i - 1) == '-') {
						total = total - currInt;
					} else if (chromosome.charAt(i - 1) == '*') {
						total = total * currInt;
					} else if (chromosome.charAt(i - 1) == '/') {
						total = total / currInt;
					}
				}
			}
		}

		return total;
	}

	/**
	 * Check whether this chromosome is a valid string format.
	 * @return true if the chromosome is valid
	 * */
	private boolean isValidChromosome() {
		boolean result = true;

		if (chromosome.length() != LENGTH) {
			return false;
		}

		for (int i = 0; i < chromosome.length(); i++) {
			// check if the generated chromosome is in valid format
			// (number operator number operator...)
			boolean isDigit = Character.isDigit(chromosome.charAt(i));
			int currInt = Character.getNumericValue(chromosome.charAt(i));
			if (!(i % 2 == 0 && isDigit) && !(i % 2 == 1 && !isDigit)) {
				result = false;
				break;
			}
			// divide by 0 exception
			if (currInt == 0 && i > 0 && chromosome.charAt(i - 1) == '/') {
				result = false;
				break;
			}
		}

		return result;
	}

	/**
	 * Mutate chromosome.
	 * */
	public final void mutate() {
		MersenneTwisterFast random = Util.getInstance().getRandomInstance();
		for (int i = 0; i < decodedChromosome.length(); i++) {
			if (random.nextDouble() <= MUTATION_RATE) {
				if (decodedChromosome.charAt(i) == '0') {
					decodedChromosome.setCharAt(i, '1');
				} else {
					decodedChromosome.setCharAt(i, '0');
				}
			}
		}
		updateChromosomeFromDecodedChromosome();
	}

	/**
	 * Update the chromosome after the decoded chromosome has been mutated
	 * or crossed over or changed in any ways.
	 * */
	private void updateChromosomeFromDecodedChromosome() {
		chromosome.delete(0, chromosome.length());
		int length = DECODED_CHARACTERS[0].length();
		for (int i = 0; i < LENGTH; i++) {
			int start = i * length;
			String string = decodedChromosome.substring(start, start + length);
			for (int j = 0; j < DECODED_CHARACTERS.length; j++) {
				if (DECODED_CHARACTERS[j].equals(string)) {
					chromosome.append(CHARACTERS[j]);
					break;
				}
			}
		}
	}

	/**
	 * Test if this chromosome is the correct answer.
	 * @return true/false
	 * */
	public final boolean isCorrectAnswer() {
		boolean result = false;
		if (fitnessScore == 0) {
			result = true;
		}
		return result;
	}

	/**
	 * @return the fitnessScore
	 */
	public final double getFitnessScore() {
		return fitnessScore;
	}

	/**
	 * @return the chromosome
	 */
	public final StringBuffer getChromosome() {
		return chromosome;
	}

	/**
	 * @return the decodedChromosome
	 */
	public final StringBuffer getDecodedChromosome() {
		return decodedChromosome;
	}

	/**
	 * @param param the decodedChromosome to set
	 */
	public final void setDecodedChromosome(final String param) {
		this.decodedChromosome.delete(0, decodedChromosome.length());
		this.decodedChromosome.append(param);
		updateChromosomeFromDecodedChromosome();
	}

}
