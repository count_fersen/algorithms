package edu.mat.alg.search.impl.genetic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ec.util.MersenneTwisterFast;
import edu.mat.alg.search.ISearch;
import edu.mat.alg.util.Util;

/**
 * Genetic algorithm implementation.
 * */
public class GeneticAlgorithmSearch implements ISearch {

	/**
	 * Total population of the chromosomes.
	 * */
	private static final int TOTAL_POPULATION = 40;

	/**
	 * This is simply the chance that two chromosomes will swap their bits.
	 * */
	private static final double CROSS_RATE = 0.7;

	/**
	 * Target value to be achieved on searching.
	 * */
	private static final int TARGET_VALUE = 23;

	/**
	 * List of chromosomes.
	 * */
	private List<Chromosome> chromosomes;

	/**
	 * Random util variable.
	 * */
	private MersenneTwisterFast random;

	/**
	 * Public constructor.
	 * */
	public GeneticAlgorithmSearch() {
		random = Util.getInstance().getRandomInstance();
		chromosomes = new ArrayList<Chromosome>();
	}

	@Override
	public final void search() {
		// generate new population
		generatePopulation();

		List<Chromosome> newPopulation = new ArrayList<Chromosome>();
		int generation = 0;

		while (true) {
			newPopulation.clear();
			generation++;
			for (int i = 0; i < TOTAL_POPULATION; i = i + 2) {
				// select two members
				Chromosome chromosome1 = selectChromosome();
				Chromosome chromosome2 = selectChromosome();

				// crossover bits
				crossOverChromosomes(chromosome1, chromosome2);

				// mutate chromosomes
				chromosome1.mutate();
				chromosome2.mutate();

				// recalculate fitness score
				chromosome1.calculateFitnessScore(TARGET_VALUE);
				chromosome2.calculateFitnessScore(TARGET_VALUE);

				if (chromosome1.isCorrectAnswer()) {
					System.out.println("Generation: " + generation + " "
							+ chromosome1.getChromosome());
					return;
				} else if (chromosome2.isCorrectAnswer()) {
					System.out.println("Generation: " + generation + " "
							+ chromosome2.getChromosome());
					return;
				} else {
					newPopulation.add(chromosome1);
					newPopulation.add(chromosome2);

					System.out.println("Chromosome: "
							+ chromosome1.getChromosome());
					System.out.println("Chromosome: "
							+ chromosome2.getChromosome());
				}
			}
			chromosomes.addAll(newPopulation);
		}
	}

	/**
	 * Generate first population.
	 * */
	private void generatePopulation() {
		for (int i = 0; i < TOTAL_POPULATION; i++) {
			Chromosome chromosome = new Chromosome(TARGET_VALUE);
			chromosomes.add(chromosome);
			
			System.out.println("Chromosome: " + chromosome.getChromosome());
		}
	}

	/**
	 * Select a chromosome from the population pool.
	 * The chance of being selected is proportional to its fitness.
	 * We use Roulette Wheel to select a chromosome.
	 * 
	 * @return the selected chromosome
	 * */
	private Chromosome selectChromosome() {
		Chromosome chromosome = null;
		double totalFitnessScore = 0.0;
		double totalSlicedScore = 0.0;
		double slice = 0.0;

		// calculate total fitness score
		Iterator<Chromosome> iterator = chromosomes.iterator();
		while (iterator.hasNext()) {
			Chromosome obj = iterator.next();
			totalFitnessScore = totalFitnessScore + obj.getFitnessScore();
		}

		// randomly put a slice
		slice = totalFitnessScore * random.nextDouble();

		// get the chromosome where slice has been put
		iterator = chromosomes.iterator();
		while (iterator.hasNext()) {
			Chromosome obj = iterator.next();
			totalSlicedScore = totalSlicedScore + obj.getFitnessScore();
			if (totalSlicedScore >= slice) {
				chromosome = obj;
				chromosomes.remove(obj);
				break;
			}
		}

		if (chromosome == null) {
			chromosome = chromosomes.remove(0);
		}

		return chromosome;
	}

	/**
	 * Crossover two chromosomes.
	 * @param chromo1 first {@link Chromosome}
	 * @param chromo2 second {@link Chromosome}
	 * */
	private void crossOverChromosomes(
			final Chromosome chromo1, final Chromosome chromo2) {
		// check if we should crossover chromosomes
		if (random.nextDouble() > CROSS_RATE) {
			return;
		}

		// generate random position
		int position = random.nextInt(chromo1.getDecodedChromosome().length());

		// swap the bits
		String part1 = chromo1.getDecodedChromosome().substring(0, position);
		String part2 = chromo1.getDecodedChromosome().substring(position);
		String part3 = chromo2.getDecodedChromosome().substring(0, position);
		String part4 = chromo2.getDecodedChromosome().substring(position);

		chromo1.setDecodedChromosome(part1 + part4);
		chromo2.setDecodedChromosome(part3 + part2);
	}

}
