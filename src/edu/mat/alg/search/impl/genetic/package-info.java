/**
 * Package contains genetic search algorithm implementations.
 * */
package edu.mat.alg.search.impl.genetic;