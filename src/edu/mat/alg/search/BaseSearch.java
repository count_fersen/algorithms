package edu.mat.alg.search;

import edu.mat.alg.search.data.City;

/**
 * Base class for all search algorithms.
 * */
public abstract class BaseSearch implements ISearch {

	/**
	 * City to start searching from.
	 * */
	private City startCity;

	/**
	 * City to get to.
	 * */
	private City finishCity;

	/**
	 * Public constructor.
	 * 
	 * @param startCityParam city to start from
	 * @param finishCityParam city to get to
	 * */
	public BaseSearch(final City startCityParam, final City finishCityParam) {
		this.startCity = startCityParam;
		this.finishCity = finishCityParam;
	}

	/**
	 * @return the startCity
	 */
	public final City getStartCity() {
		return startCity;
	}

	/**
	 * @param param
	 *            the startCity to set
	 */
	public final void setStartCity(final City param) {
		this.startCity = param;
	}

	/**
	 * @return the finishCity
	 */
	public final City getFinishCity() {
		return finishCity;
	}

	/**
	 * @param param
	 *            the finishCity to set
	 */
	public final void setFinishCity(final City param) {
		this.finishCity = param;
	}

	/**
	 * Compare a {@link City} with destination {@link City}.
	 * 
	 * @param city the {@link City} to be compared
	 * @return true if it is destination {@link City}
	 * */
	protected final boolean compareCity(final City city) {
		if (city == getFinishCity()) {
			return true;
		} else if (city.getCityName().equals(getFinishCity().getCityName())) {
			return true;
		}
		return false;
	}

}
