package edu.mat.alg.search;

/**
 * Interface for all search algorithms.
 * */
public interface ISearch {

	/**
	 * Search algorithm implementation.
	 * */
	void search();
}
