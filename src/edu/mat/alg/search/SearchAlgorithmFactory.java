package edu.mat.alg.search;

import edu.mat.alg.search.data.City;
import edu.mat.alg.search.impl.BestFirstSearch;
import edu.mat.alg.search.impl.BreadthFirstSearch;
import edu.mat.alg.search.impl.DepthFirstSearch;
import edu.mat.alg.search.impl.genetic.GeneticAlgorithmSearch;
import edu.mat.alg.search.impl.genetic.tsp.TravellingSalesmanProblem;

/**
 * Factory class to get search algorithm.
 * */
public final class SearchAlgorithmFactory {

	/**
	 * Breadth-first search algorithm.
	 * */
	public static final int BREADTH_FIRST_SEARCH = 0;

	/**
	 * Depth-first search algorithm.
	 * */
	public static final int DEPTH_FIRST_SEARCH = 1;

	/**
	 * Best-first search algorithm.
	 * */
	public static final int BEST_FIRST_SEARCH = 2;

	/**
	 * Genetic algorithm.
	 * */
	public static final int GENETIC_ALGORITHM = 3;

	/**
	 * Travelling salesman problem using genetic algorithm.
	 * */
	public static final int TRAVELLING_SALESMAN_PROBLEM = 4;

	/**
	 * Single instance of the class.
	 * */
	private static final SearchAlgorithmFactory INSTANCE =
		new SearchAlgorithmFactory();

	/**
	 * Private constructor.
	 * */
	private SearchAlgorithmFactory() {
		// do nothing
	}

	/**
	 * Get instance of the class.
	 * 
	 * @return {@link SearchAlgorithmFactory}
	 * */
	public static SearchAlgorithmFactory getInstance() {
		return INSTANCE;
	}

	/**
	 * Get search algorithm object based on the parameter.
	 * 
	 * @return the implementation of {@link ISearch} algorithm
	 * @param algorithmType type of the algorithm
	 * @param cities array of all cities on map
	 * */
	public ISearch getSearchAlgorithm(
			final int algorithmType,
			final City[] cities) {
		ISearch result = null;
		switch (algorithmType) {
			case BREADTH_FIRST_SEARCH:
				result = new BreadthFirstSearch(cities[0], cities[1]);
				break;
			case DEPTH_FIRST_SEARCH:
				result = new DepthFirstSearch(cities[0], cities[1]);
				break;
			case BEST_FIRST_SEARCH:
				result = new BestFirstSearch(cities[0], cities[1]);
				break;
			case GENETIC_ALGORITHM:
				result = new GeneticAlgorithmSearch();
				break;
			case TRAVELLING_SALESMAN_PROBLEM:
				result = new TravellingSalesmanProblem(cities);
				break;
			default:
				break;
		}
		return result;
	}

}
