/**
 * Package contains data for search algorithm.
 * */
package edu.mat.alg.search.data;