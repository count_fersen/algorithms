package edu.mat.alg.search.data;

/**
 * Node object model.
 * */
public class Node {

	/**
	 * {@link City} to start from.
	 * */
	private City cityFrom;

	/**
	 * {@link City} to get to.
	 * */
	private City cityTo;

	/**
	 * Length between two cities.
	 * */
	private Double length;

	/**
	 * Node is expanded once it has been searched.
	 * */
	private boolean expanded;

	/**
	 * @return the cityFrom
	 */
	public final City getCityFrom() {
		return cityFrom;
	}

	/**
	 * @param param
	 *            the cityFrom to set
	 */
	public final void setCityFrom(final City param) {
		this.cityFrom = param;
	}

	/**
	 * @return the cityTo
	 */
	public final City getCityTo() {
		return cityTo;
	}

	/**
	 * @param param
	 *            the cityTo to set
	 */
	public final void setCityTo(final City param) {
		this.cityTo = param;
	}

	/**
	 * @return the length
	 */
	public final Double getLength() {
		return length;
	}

	/**
	 * @param param
	 *            the length to set
	 */
	public final void setLength(final Double param) {
		this.length = param;
	}

	/**
	 * @return the expanded
	 */
	public final boolean isExpanded() {
		return expanded;
	}

	/**
	 * @param param
	 *            the expanded to set
	 */
	public final void setExpanded(final boolean param) {
		this.expanded = param;
	}

}
