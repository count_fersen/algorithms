package edu.mat.alg.search.data;

import java.util.List;

/**
 * City object model.
 * */
public class City {

	/**
	 * Name of the city.
	 * */
	private String cityName;

	/**
	 * Nodes that can be reached from this City.
	 * */
	private List<Node> nodes;

	/**
	 * @return the cityName
	 */
	public final String getCityName() {
		return cityName;
	}

	/**
	 * @param param
	 *            the cityName to set
	 */
	public final void setCityName(final String param) {
		this.cityName = param;
	}

	/**
	 * @return the nodes
	 */
	public final List<Node> getNodes() {
		return nodes;
	}

	/**
	 * @param param the nodes to set
	 */
	public final void setNodes(final List<Node> param) {
		this.nodes = param;
	}

}
