package edu.mat.alg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ec.util.MersenneTwisterFast;
import edu.mat.alg.datastructure.binarysearchtree.BinarySearchTree;
import edu.mat.alg.rulesystem.BackwardChaining;
import edu.mat.alg.rulesystem.ForwardChaining;
import edu.mat.alg.rulesystem.data.Clause;
import edu.mat.alg.rulesystem.data.ClauseOperator;
import edu.mat.alg.rulesystem.data.Rule;
import edu.mat.alg.rulesystem.data.RuleSign;
import edu.mat.alg.search.ISearch;
import edu.mat.alg.search.SearchAlgorithmFactory;
import edu.mat.alg.search.data.City;
import edu.mat.alg.search.data.Node;
import edu.mat.alg.util.Util;

/**
 * Main class to start application.
 * */
public final class Main {

	/**
	 * Private constructor.
	 * */
	private Main() {
	}

	/**
	 * Main method.
	 * 
	 * @param args
	 *            the arguments
	 * */
	public static void main(final String[] args) {
		City[] cities = getAllCities();
		SearchAlgorithmFactory factory = SearchAlgorithmFactory.getInstance();
		ISearch alg = factory.getSearchAlgorithm(
				SearchAlgorithmFactory.TRAVELLING_SALESMAN_PROBLEM,
				cities);
		alg.search();

		System.out.println("Forward Chaining");

		ForwardChaining forwardChaining = new ForwardChaining(
				getRules(), getAvailableFacts(true));
		forwardChaining.run();

		System.out.println("Backward Chaining");

		BackwardChaining backwardChaining = new BackwardChaining(
				getRules(), getAvailableFacts(false));
		backwardChaining.run();

		System.out.println("Binary Search Tree");

		BinarySearchTree tree = new BinarySearchTree();
		tree.buildTree(getBinarySearchTreeData());
		System.out.println("Root node      : " + tree.getRoot().getValue());
		System.out.println("Most right node: "
				+ tree.getMostRightNode(tree.getRoot()).getValue());
		System.out.println("Most left node : "
				+ tree.getMostLeftNode(tree.getRoot()).getValue());
		tree.printTree(tree.getRoot(), new StringBuffer());
		tree.deleteNode(6);
		tree.printTree(tree.getRoot(), new StringBuffer());
	}

	/**
	 * Get all cities to search.
	 * 
	 * @return an array that contains all cities in map
	 * */
	private static City[] getAllCities() {
		City surabaya = new City();
		surabaya.setCityName("Surabaya");
		surabaya.setNodes(new ArrayList<Node>());

		City malang = new City();
		malang.setCityName("Malang");
		malang.setNodes(new ArrayList<Node>());

		City kediri = new City();
		kediri.setCityName("Kediri");
		kediri.setNodes(new ArrayList<Node>());

		City sidoarjo = new City();
		sidoarjo.setCityName("Sidoarjo");
		sidoarjo.setNodes(new ArrayList<Node>());

		City krian = new City();
		krian.setCityName("Rakrian");
		krian.setNodes(new ArrayList<Node>());

		MersenneTwisterFast random = Util.getInstance().getRandomInstance();

		Node nodeSurabayaMalang = new Node();
		nodeSurabayaMalang.setCityFrom(surabaya);
		nodeSurabayaMalang.setCityTo(malang);
		nodeSurabayaMalang.setLength(random.nextDouble());
		nodeSurabayaMalang.setExpanded(false);

		Node nodeSurabayaKediri = new Node();
		nodeSurabayaKediri.setCityFrom(surabaya);
		nodeSurabayaKediri.setCityTo(kediri);
		nodeSurabayaKediri.setLength(random.nextDouble());
		nodeSurabayaKediri.setExpanded(false);

		Node nodeSurabayaSidoarjo = new Node();
		nodeSurabayaSidoarjo.setCityFrom(surabaya);
		nodeSurabayaSidoarjo.setCityTo(sidoarjo);
		nodeSurabayaSidoarjo.setLength(random.nextDouble());
		nodeSurabayaSidoarjo.setExpanded(false);

		Node nodeSurabayaKrian = new Node();
		nodeSurabayaKrian.setCityFrom(surabaya);
		nodeSurabayaKrian.setCityTo(krian);
		nodeSurabayaKrian.setLength(random.nextDouble());
		nodeSurabayaKrian.setExpanded(false);

		Node nodeKrianKediri = new Node();
		nodeKrianKediri.setCityFrom(krian);
		nodeKrianKediri.setCityTo(kediri);
		nodeKrianKediri.setLength(random.nextDouble());
		nodeKrianKediri.setExpanded(false);

		Node nodeKediriSidoarjo = new Node();
		nodeKediriSidoarjo.setCityFrom(kediri);
		nodeKediriSidoarjo.setCityTo(sidoarjo);
		nodeKediriSidoarjo.setLength(random.nextDouble());
		nodeKediriSidoarjo.setExpanded(false);

		Node nodeSidoarjoMalang = new Node();
		nodeSidoarjoMalang.setCityFrom(sidoarjo);
		nodeSidoarjoMalang.setCityTo(malang);
		nodeSidoarjoMalang.setLength(random.nextDouble());
		nodeSidoarjoMalang.setExpanded(false);

		surabaya.getNodes().add(nodeSurabayaMalang);
		surabaya.getNodes().add(nodeSurabayaKediri);
		surabaya.getNodes().add(nodeSurabayaSidoarjo);
		surabaya.getNodes().add(nodeSurabayaKrian);

		krian.getNodes().add(nodeKrianKediri);
		krian.getNodes().add(nodeSurabayaKrian);

		kediri.getNodes().add(nodeSurabayaKediri);
		kediri.getNodes().add(nodeKrianKediri);
		kediri.getNodes().add(nodeKediriSidoarjo);

		sidoarjo.getNodes().add(nodeKediriSidoarjo);
		sidoarjo.getNodes().add(nodeSurabayaSidoarjo);
		sidoarjo.getNodes().add(nodeSidoarjoMalang);

		malang.getNodes().add(nodeSurabayaMalang);
		malang.getNodes().add(nodeSidoarjoMalang);

		return new City[] {surabaya, sidoarjo, malang, kediri, krian};
	}

	/**
	 * Get list of {@link Rule} for rule-based system.
	 * 
	 * @return an array of {@link Rule}s
	 * */
	private static Set<Rule> getRules() {
		Clause cycle = new Clause("vehicleType", ClauseOperator.EQUAL, "cycle");
		Clause automobile = new Clause(
				"vehicleType", ClauseOperator.EQUAL, "automobile");
		Clause motorNo = new Clause("motor", ClauseOperator.EQUAL, "no");
		Clause motorYes = new Clause("motor", ClauseOperator.EQUAL, "yes");
		Clause vBicycle = new Clause(
				"vehicle", ClauseOperator.EQUAL, "Bicycle");
		Clause vTricycle = new Clause(
				"vehicle", ClauseOperator.EQUAL, "Tricycle");
		Clause vMotorcycle = new Clause(
				"vehicle", ClauseOperator.EQUAL, "Motorcycle");
		Clause vSportCar = new Clause(
				"vehicle", ClauseOperator.EQUAL, "Sport Car");
		Clause vSedan = new Clause("vehicle", ClauseOperator.EQUAL, "Sedan");
		Clause vMinivan = new Clause(
				"vehicle", ClauseOperator.EQUAL, "Minivan");
		Clause vSUV = new Clause("vehicle", ClauseOperator.EQUAL, "SUV");
		Clause wheel2 = new Clause("num_wheels", ClauseOperator.EQUAL, "2");
		Clause wheel3 = new Clause("num_wheels", ClauseOperator.EQUAL, "3");
		Clause wheel4 = new Clause("num_wheels", ClauseOperator.EQUAL, "4");
		Clause wheelLessThan4 = new Clause(
				"num_wheels", ClauseOperator.LESS_THAN, "4");
		Clause door2 = new Clause("num_doors", ClauseOperator.EQUAL, "2");
		Clause door3 = new Clause("num_doors", ClauseOperator.EQUAL, "3");
		Clause door4 = new Clause("num_doors", ClauseOperator.EQUAL, "4");
		Clause small = new Clause("size", ClauseOperator.EQUAL, "small");
		Clause medium = new Clause("size", ClauseOperator.EQUAL, "medium");
		Clause large = new Clause("size", ClauseOperator.EQUAL, "large");

		Rule bicycle = new Rule("Bicycle");
		bicycle.addSign(RuleSign.IF);
		bicycle.addClause(cycle);
		bicycle.addSign(RuleSign.AND);
		bicycle.addClause(wheel2);
		bicycle.addSign(RuleSign.AND);
		bicycle.addClause(motorNo);
		bicycle.addSign(RuleSign.THEN);
		bicycle.addClause(vBicycle);

		Rule tricycle = new Rule("Tricycle");
		tricycle.addSign(RuleSign.IF);
		tricycle.addClause(cycle);
		tricycle.addSign(RuleSign.AND);
		tricycle.addClause(wheel3);
		tricycle.addSign(RuleSign.AND);
		tricycle.addClause(motorNo);
		tricycle.addSign(RuleSign.THEN);
		tricycle.addClause(vTricycle);

		Rule motorcycle = new Rule("Motorcycle");
		motorcycle.addSign(RuleSign.IF);
		motorcycle.addClause(cycle);
		motorcycle.addSign(RuleSign.AND);
		motorcycle.addClause(wheel2);
		motorcycle.addSign(RuleSign.AND);
		motorcycle.addClause(motorYes);
		motorcycle.addSign(RuleSign.THEN);
		motorcycle.addClause(vMotorcycle);

		Rule sportCar = new Rule("SportCar");
		sportCar.addSign(RuleSign.IF);
		sportCar.addClause(automobile);
		sportCar.addSign(RuleSign.AND);
		sportCar.addClause(small);
		sportCar.addSign(RuleSign.AND);
		sportCar.addClause(door2);
		sportCar.addSign(RuleSign.THEN);
		sportCar.addClause(vSportCar);

		Rule sedan = new Rule("Sedan");
		sedan.addSign(RuleSign.IF);
		sedan.addClause(automobile);
		sedan.addSign(RuleSign.AND);
		sedan.addClause(medium);
		sedan.addSign(RuleSign.AND);
		sedan.addClause(door4);
		sedan.addSign(RuleSign.THEN);
		sedan.addClause(vSedan);

		Rule minivan = new Rule("Minivan");
		minivan.addSign(RuleSign.IF);
		minivan.addClause(automobile);
		minivan.addSign(RuleSign.AND);
		minivan.addClause(medium);
		minivan.addSign(RuleSign.AND);
		minivan.addClause(door3);
		minivan.addSign(RuleSign.THEN);
		minivan.addClause(vMinivan);

		Rule suv = new Rule("SUV");
		suv.addSign(RuleSign.IF);
		suv.addClause(automobile);
		suv.addSign(RuleSign.AND);
		suv.addClause(large);
		suv.addSign(RuleSign.AND);
		suv.addClause(door4);
		suv.addSign(RuleSign.THEN);
		suv.addClause(vSUV);

		Rule ruleCycle = new Rule("Cycle");
		ruleCycle.addSign(RuleSign.IF);
		ruleCycle.addClause(wheelLessThan4);
		ruleCycle.addSign(RuleSign.THEN);
		ruleCycle.addClause(cycle);

		Rule ruleAutomobile = new Rule("Automobile");
		ruleAutomobile.addSign(RuleSign.IF);
		ruleAutomobile.addClause(wheel4);
		ruleAutomobile.addSign(RuleSign.AND);
		ruleAutomobile.addClause(motorYes);
		ruleAutomobile.addSign(RuleSign.THEN);
		ruleAutomobile.addClause(automobile);

		Set<Rule> rules = new HashSet<Rule>();
		rules.add(bicycle);
		rules.add(tricycle);
		rules.add(motorcycle);
		rules.add(sportCar);
		rules.add(sedan);
		rules.add(minivan);
		rules.add(suv);
		rules.add(ruleCycle);
		rules.add(ruleAutomobile);

		return rules;
	}

	/**
	 * Get initial facts for rule-based system.
	 * 
	 * @param isForwardChainingFacts flag whether returning facts
	 *        for forward-chaining or backward-chaining
	 * @return key-value pair of facts
	 * */
	private static Map<String, String> getAvailableFacts(
			final boolean isForwardChainingFacts) {
		Map<String, String> result = new HashMap<String, String>();

		if (isForwardChainingFacts) {
			result.put("num_wheels", "4");
			result.put("motor", "yes");
			result.put("num_doors", "3");
			result.put("size", "medium");
		} else {
			result.put("vehicle", "Minivan");
		}

		return result;
	}

	/**
	 * Get data for binary search tree.
	 * @return array of numbers
	 * */
	private static List<Integer> getBinarySearchTreeData() {
		List<Integer> result = new ArrayList<Integer>();
		result.add(3);
		result.add(5);
		result.add(6);
		result.add(7);
		result.add(10);
		result.add(12);
		result.add(13);
		result.add(15);
		result.add(16);
		result.add(18);
		result.add(20);
		result.add(23);
		return result;
	}

}
