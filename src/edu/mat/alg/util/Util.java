package edu.mat.alg.util;

import ec.util.MersenneTwisterFast;

/**
 * Utility class.
 * */
public final class Util {

	/**
	 * Single instance.
	 * */
	private static final Util INSTANCE = new Util();

	/**
	 * MersenneTwisterFast random implementation.
	 * */
	private MersenneTwisterFast random;

	/**
	 * Private constructor.
	 * */
	private Util() {
	}

	/**
	 * Get instance of this class.
	 * @return instance of this class
	 * */
	public static Util getInstance() {
		return INSTANCE;
	}

	/**
	 * Get MersenneTwisterFast instance.
	 * @return {@link MersenneTwisterFast}
	 * */
	public MersenneTwisterFast getRandomInstance() {
		if (random == null) {
			random = new MersenneTwisterFast();
		}
		return random;
	}

}
