package edu.mat.alg.rulesystem;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import edu.mat.alg.rulesystem.data.Rule;

/**
 * Base class for all rule-based system implementation.
 * */
public abstract class BaseRuleSystem implements IRuleSystem {

	/**
	 * Set of {@link Rule}.
	 * */
	private Set<Rule> rules;

	/**
	 * Key-value pair of known facts.
	 * */
	private Map<String, String> facts;

	/**
	 * @param paramRule a set of {@link Rule}
	 * @param paramFacts key-value pairs of initial facts
	 * */
	public BaseRuleSystem(
			final Set<Rule> paramRule,
			final Map<String, String> paramFacts) {
		super();
		this.rules = paramRule;
		this.facts = paramFacts;
	}

	/**
	 * Check whether all {@link Rule}s are valid.
	 * @return true/false
	 * */
	public final boolean isValidRules() {
		boolean result = true;

		Iterator<Rule> iter = rules.iterator();
		while (iter.hasNext()) {
			Rule rule = iter.next();
			if (!rule.isValidRule()) {
				result = false;
				break;
			}
		}

		return result;
	}

	/**
	 * @return the rules
	 */
	protected final Set<Rule> getRules() {
		return rules;
	}

	/**
	 * @return the facts
	 */
	protected final Map<String, String> getFacts() {
		return facts;
	}

}
