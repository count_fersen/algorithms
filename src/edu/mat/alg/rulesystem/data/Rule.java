package edu.mat.alg.rulesystem.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Rule object model. Example of a Rule is:
 * <br><br>
 * Bicycle: IF vehicleType = cycle <br>
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; AND numWheels = 2 <br>
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; AND motor = NO <br>
 * &nbsp; &nbsp; &nbsp; &nbsp; THEN vehicle = Bicycle <br>
 * */
public class Rule {

	/**
	 * Name of the Rule.
	 * */
	private String ruleName;

	/**
	 * List of {@link Clause} in this Rule. Total number of clauses is equal to
	 * total number of signs.
	 * */
	private List<Clause> clauses;

	/**
	 * List of {@link RuleSign} in this Rule. Total number of signs is equal to
	 * total number of clauses.
	 * */
	private List<RuleSign> signs;

	/**
	 * A flag to check whether this Rule has been fired or not.
	 * */
	private boolean fired;

	/**
	 * @param name name of the Rule
	 * */
	public Rule(final String name) {
		this.ruleName = name;
		this.clauses = new ArrayList<Clause>();
		this.signs = new ArrayList<RuleSign>();
	}

	/**
	 * @return the fired
	 */
	public final boolean isFired() {
		return fired;
	}

	/**
	 * @return the clauses
	 */
	public final List<Clause> getClauses() {
		return clauses;
	}

	/**
	 * @return the signs
	 */
	public final List<RuleSign> getSigns() {
		return signs;
	}

	/**
	 * @return the ruleName
	 */
	public final String getRuleName() {
		return ruleName;
	}

	/**
	 * Add a {@link Clause} to this Rule.
	 * @param clause the {@link Clause} to add
	 * */
	public final void addClause(final Clause clause) {
		clauses.add(clause);
	}

	/**
	 * Add a sign to this Rule. The values are defined in enum {@link RuleSign}.
	 * @param sign the {@link RuleSign} to add
	 * */
	public final void addSign(final RuleSign sign) {
		signs.add(sign);
	}

	/**
	 * Check whether this Rule is valid.
	 * @return true/false
	 * */
	public final boolean isValidRule() {
		boolean result = true;

		// at least one IF and one THEN clause, first sign must be IF,
		// and last sign must be THEN
		if (clauses.size() != signs.size()
				|| clauses.size() < 2
				|| signs.get(0) != RuleSign.IF
				|| signs.get(signs.size() - 1) != RuleSign.THEN) {
			result = false;
		}

		// from index 1 to size-1 must not contain IF-THEN sign
		if (result) {
			for (int i = 1; i < signs.size() - 1; i++) {
				RuleSign sign = signs.get(i);
				if (sign == RuleSign.IF || sign == RuleSign.THEN) {
					result = false;
					break;
				}
			}
		}

		// last clause's operator must be EQUAL
		Clause clause = clauses.get(clauses.size() - 1);
		if (clause.getClauseOperator() != ClauseOperator.EQUAL) {
			result = false;
		}

		return result;
	}

	/**
	 * Fire the Rule. Firing a Rule will make the THEN Clause to be put
	 * in the facts.
	 * 
	 * @param facts the currently known facts
	 * */
	public final void fireRule(final Map<String, String> facts) {
		fired = true;

		Clause clause = clauses.get(clauses.size() - 1);
		facts.put(clause.getClauseVariable(), clause.getClauseValue());

		System.out.println("Rule " + ruleName + " has been fired.");
	}

	/**
	 * Fire the Rule in backward mode. Firing a Rule in backward mode will
	 * make all ANTECEDENT Clauses to be put in the facts.
	 * 
	 * @param facts the currently known facts
	 * */
	public final void fireRuleInBackwardMode(final Map<String, String> facts) {
		fired = true;

		Iterator<Clause> iter = clauses.subList(
				0,
				clauses.size() - 1).iterator();
		while (iter.hasNext()) {
			Clause clause = iter.next();
			facts.put(clause.getClauseVariable(), clause.getClauseValue());
		}

		System.out.println("Rule " + ruleName + " has been fired.");
	}

}
