/**
 * Package contains rule-based system's data models.
 * */
package edu.mat.alg.rulesystem.data;