package edu.mat.alg.rulesystem.data;

/**
 * Clause object model. Example of a Clause is:
 * <br><br>
 * <b>'vehicleType = cycle'</b>,
 * <b>'numWheels = 2'</b>,
 * <b>'motor = NO'</b>,
 * <b>'vehicle = Bicycle'</b>
 * <br><br>
 * A Clause can only has a variable, an operator, and a value.
 * From clause <b>'vehicleType = cycle'</b> we know that
 * <b>'vehicleType'</b> is a variable, <b>'='</b> is an operator,
 * and <b>'cycle'</b> is the value.
 * */
public class Clause {

	/**
	 * Variable part of the Clause.
	 * */
	private String clauseVariable;

	/**
	 * Operator part of the Clause.
	 * */
	private ClauseOperator clauseOperator;

	/**
	 * Value part of the Clause.
	 * */
	private String clauseValue;

	/**
	 * @param variable the variable part of the Clause
	 * @param operator the operator part of the Clause.
	 *        The values are defined in {@link ClauseOperator}
	 * @param value the value part of the Clause
	 * */
	public Clause(
			final String variable,
			final ClauseOperator operator,
			final String value) {
		this.clauseVariable = variable;
		this.clauseOperator = operator;
		this.clauseValue = value;
	}

	/**
	 * Evaluate the Clause. For example x > 3 will be evaluated by
	 * substituting x with the value parameter.
	 * 
	 * @param value the value for the clause's variable
	 * @return true/false
	 * */
	public final boolean evaluateClause(final String value) {
		boolean result = false;

		switch (clauseOperator) {
			case EQUAL:
				result = value.equals(clauseValue);
				break;
			case GREATER_THAN:
				try {
					Double first = Double.parseDouble(value);
					Double second = Double.parseDouble(clauseValue);
					result = (first > second);
				} catch (NumberFormatException e) {
					result = false;
				}
				break;
			case LESS_THAN:
				try {
					Double first = Double.parseDouble(value);
					Double second = Double.parseDouble(clauseValue);
					result = (first < second);
				} catch (NumberFormatException e) {
					result = false;
				}
				break;
			case GREATER_OR_EQUAL_THAN:
				try {
					Double first = Double.parseDouble(value);
					Double second = Double.parseDouble(clauseValue);
					result = (first >= second);
				} catch (NumberFormatException e) {
					result = false;
				}
				break;
			case LESS_OR_EQUAL_THAN:
				try {
					Double first = Double.parseDouble(value);
					Double second = Double.parseDouble(clauseValue);
					result = (first <= second);
				} catch (NumberFormatException e) {
					result = false;
				}
				break;
			default:
				break;
		}

		return result;
	}

	/**
	 * @return the clauseVariable
	 */
	public final String getClauseVariable() {
		return clauseVariable;
	}

	/**
	 * @return the clauseOperator
	 */
	public final ClauseOperator getClauseOperator() {
		return clauseOperator;
	}

	/**
	 * @return the clauseValue
	 */
	public final String getClauseValue() {
		return clauseValue;
	}

}
