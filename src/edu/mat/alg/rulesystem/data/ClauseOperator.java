package edu.mat.alg.rulesystem.data;

/**
 * An enum class that define available operators for a {@link Clause}.
 * */
public enum ClauseOperator {

	/** '=' operator. */
	EQUAL,

	/** '<' operator. */
	LESS_THAN,

	/** '>' operator. */
	GREATER_THAN,

	/** '<=' operator. */
	LESS_OR_EQUAL_THAN,

	/** '>=' operator. */
	GREATER_OR_EQUAL_THAN;

}
