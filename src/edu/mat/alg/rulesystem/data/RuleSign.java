package edu.mat.alg.rulesystem.data;

/**
 * An enum class that define available signs for a {@link Rule}.
 * */
public enum RuleSign {

	/** AND Sign. */
	AND,

	/** OR Sign. */
	OR,

	/** IF Sign. */
	IF,

	/** THEN Sign. */
	THEN;

}
