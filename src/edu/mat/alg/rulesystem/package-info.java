/**
 * Package contains rule-based system algorithms.
 * */
package edu.mat.alg.rulesystem;