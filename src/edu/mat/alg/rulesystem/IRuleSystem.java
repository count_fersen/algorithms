package edu.mat.alg.rulesystem;

import edu.mat.alg.rulesystem.data.Rule;

/**
 * Interface for all rule-based system implementations.
 * */
public interface IRuleSystem {

	/**
	 * Run the rule system implementation.
	 * */
	void run();

	/**
	 * Check whether all Rules are valid.
	 * 
	 * @return true/false
	 * */
	boolean isValidRules();

	/**
	 * Evaluate a {@link Rule}.
	 * 
	 * @return true/false evaluation condition
	 * @param rule the {@link Rule} to be evaluated
	 * */
	boolean evaluateRule(Rule rule);

}
