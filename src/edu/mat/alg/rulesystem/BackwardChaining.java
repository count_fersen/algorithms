package edu.mat.alg.rulesystem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.mat.alg.rulesystem.data.Clause;
import edu.mat.alg.rulesystem.data.Rule;

/**
 * Backward Chaining implementation. We use recursive instead of following
 * bigus book explanation since recursive is more intuitive to follow.
 * */
public class BackwardChaining extends BaseRuleSystem {

	/**
	 * @param paramRule a set of {@link Rule}
	 * @param paramFacts key-value pairs of initial facts
	 * */
	public BackwardChaining(
			final Set<Rule> paramRule,
			final Map<String, String> paramFacts) {
		super(paramRule, paramFacts);
	}

	/**
	 * Run the backward-chaning.
	 * */
	public final void run() {
		// check if all rules are valid
		isValidRules();

		System.out.println("Known Facts: " + getFacts());

		Set<String> keys = getFacts().keySet();
		Iterator<String> iter = keys.iterator();

		// match & evaluate first Rule
		Rule rule = matchRules(iter.next());
		if (rule != null && evaluateRule(rule)) {
			System.out.println("Facts correct.");
		} else {
			System.out.println("Facts incorrect!");
		}
		
	}

	/**
	 * Match {@link Rule}s against data in goalVariable
	 * and put them into the goalStack.
	 * 
	 * @param goalVariable the goal variable to match
	 * @return the matched {@link Rule}
	 * */
	private Rule matchRules(final String goalVariable) {
		Rule result = null;

		String value = getFacts().get(goalVariable);
		Iterator<Rule> iter = getRules().iterator();
		while (iter.hasNext()) {
			Rule rule = iter.next();
			Clause lastClause = rule.getClauses().get(
					rule.getClauses().size() - 1);
			if (!rule.isFired() && lastClause.evaluateClause(value)) {
				result = rule;
				break;
			}
		}

		return result;
	}

	/**
	 * Evaluate a {@link Rule}.
	 * 
	 * @param rule the {@link Rule} to be evaluated
	 * @return true/false condition
	 * */
	public final boolean evaluateRule(final Rule rule) {
		boolean result = false;

		List<Clause> list = rule.getClauses().subList(
				0,
				rule.getClauses().size() - 1);
		int index = 1;

		result = checkClause(list.get(0));
		for (int i = 1; i < list.size(); i++) {
			switch (rule.getSigns().get(index++)) {
				case AND:
					result = (result && checkClause(list.get(i)));
					break;
				case OR:
					result = (result || checkClause(list.get(i)));
					break;
				default:
					result = false;
					break;
			}
		}

		if (result) {
			rule.fireRuleInBackwardMode(getFacts());
			System.out.println("Known Facts: " + getFacts());
		}

		return result;
	}

	/**
	 * Check a {@link Clause} whether it is true/false.
	 * 
	 * @return true/false value
	 * @param clause the {@link Clause} to be checked
	 * */
	private boolean checkClause(final Clause clause) {
		boolean result = false;

		String variable = clause.getClauseVariable();
		if (getFacts().containsKey(variable)) {
			if (clause.evaluateClause(getFacts().get(variable))) {
				result = true;
			} else {
				result = false;
			}
		} else {
			getFacts().put(clause.getClauseVariable(), clause.getClauseValue());
			Rule rule = matchRules(variable);
			if (rule != null) {
				result = evaluateRule(rule);
			} else {
				String value = askUser(variable);
				if (clause.getClauseValue().equals(value)) {
					result = true;
				} else {
					result = false;
				}
			}
		}

		return result;
	}

	/**
	 * Get a value for a variable from user.
	 * 
	 * @param variable the variable of the value
	 * @return the value for the variable
	 * */
	private String askUser(final String variable) {
		String value = null;
		System.out.print("Please input value for " + variable + " ");
		BufferedReader bufferRead = new BufferedReader(
				new InputStreamReader(System.in));
	    try {
			value = bufferRead.readLine();
			getFacts().put(variable, value);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return value;
	}

}
