package edu.mat.alg.rulesystem;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.mat.alg.rulesystem.data.Clause;
import edu.mat.alg.rulesystem.data.Rule;

/**
 * Forward Chaining implementation.
 * */
public class ForwardChaining extends BaseRuleSystem {

	/**
	 * A Rule in conflictSet/goalStack that has been selected and about
	 * to be fired.
	 * */
	private Rule candidateRule;

	/**
	 * A Set of ready-to-fire {@link Rule}. If this is empty,
	 * the forward chaining is complete.
	 * */
	private Set<Rule> conflictSet;

	/**
	 * @param paramRule a set of {@link Rule}
	 * @param paramFacts key-value pairs of initial facts
	 * */
	public ForwardChaining(
			final Set<Rule> paramRule,
			final Map<String, String> paramFacts) {
		super(paramRule, paramFacts);
		this.conflictSet = new HashSet<Rule>();
	}

	/**
	 * Run the forward-chaning.
	 * */
	public final void run() {
		// check if all rules are valid
		isValidRules();

		System.out.println("Known Facts: " + getFacts());

		// match a Rule
		matchRules();

		// loop until conflictSet is empty which means there is
		// no more Rule to be fired
		while (!conflictSet.isEmpty()) {
			// select a Rule in conflictSet
			selectRule();

			// fire the selected Rule
			act();

			// do match Rules again
			matchRules();

			System.out.println("Known Facts: " + getFacts());
		}
	}

	/**
	 * Match {@link Rule}s against data in facts and put them
	 * into the conflictSet.
	 * */
	private void matchRules() {
		Iterator<Rule> iter = getRules().iterator();
		while (iter.hasNext()) {
			Rule rule = iter.next();
			if (!rule.isFired() && evaluateRule(rule)) {
				conflictSet.add(rule);
			}
		}
	}

	/**
	 * Evaluate antecedents of this Rule. If all antecedents are satisfied
	 * then return true, which means the Rule could be fired if it has not
	 * been fired.
	 * 
	 * @param rule the rule to be evaluated
	 * @return true/false result
	 * */
	public final boolean evaluateRule(final Rule rule) {
		boolean result = true;

		if (rule.isValidRule()) {
			List<Boolean> antecedentResults = new ArrayList<Boolean>();

			// get all antecedent clauses, evaluate each clause,
			// and then put them in a list
			Iterator<Clause> iter = rule.getClauses().subList(
					0,
					rule.getClauses().size() - 1).iterator();
			while (iter.hasNext()) {
				Clause clause = iter.next();
				String fact = clause.getClauseVariable();
				if (getFacts().containsKey(fact)) {
					antecedentResults.add(
							clause.evaluateClause(getFacts().get(fact)));
				} else {
					result = false;
					break;
				}
			}

			// evaluate all antecedent clause's result with its sign
			if (result) {
				int index = 1;
				result = antecedentResults.get(0);
				for (int i = 1; i < antecedentResults.size(); i++) {
					switch (rule.getSigns().get(index++)) {
						case AND:
							result = (result && antecedentResults.get(i));
							break;
						case OR:
							result = (result || antecedentResults.get(i));
							break;
						default:
							result = false;
							break;
					}
				}
			}
		}

		return result;
	}

	/**
	 * Select a single {@link Rule} from the conflictSet using
	 * 'conflict resolution' procedure. Conflict resolution can be achieved
	 * by assigning priority to a Rule. Or the simplest one is by picking
	 * the first Rule in the conflictSet.
	 * */
	private void selectRule() {
		candidateRule = null;

		Iterator<Rule> iter = conflictSet.iterator();
		while (iter.hasNext()) {
			candidateRule = iter.next();
			break;
		}
	}

	/**
	 * Fire the selected {@link Rule}.
	 * */
	private void act() {
		if (candidateRule != null) {
			candidateRule.fireRule(getFacts());
			conflictSet.remove(candidateRule);
		}
	}

}
